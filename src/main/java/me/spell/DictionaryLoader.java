package me.spell;

import java.util.Map;

/**
 * @author bruno
 */
public interface DictionaryLoader {
    /**
     * Load the whole dictionary into memory in a map of words to frequencies.
     * @return a Map of words to their frequencies
     */
    public Map<String, Float> load();
}
