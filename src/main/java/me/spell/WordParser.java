package me.spell;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author bruno
 */
public class WordParser {
    private final CharSet locale;

    @Inject
    public WordParser() {
        this(CharSet.ENGLISH);
    }

    WordParser(CharSet charSet) {
        this.locale = charSet;
    }

    public @Nonnull List<String> parseText(String text) {
        checkNotNull(text);
        // Take a guess of the number of words we'll find. length / 10 should give us a safe lower bound on the amount
        // of words we'll get for most typical texts that we'll encounter.
        List<String> words = new ArrayList<>(Math.max(2, text.length() / 10));
        Matcher wordMatcher = locale.pattern.matcher(text.toLowerCase());

        while (wordMatcher.find()) words.add(wordMatcher.group());

        return words;
    }

    public enum CharSet {
        ENGLISH("[a-z']+");

        final Pattern pattern;

        CharSet(String matcher) {
            this.pattern = Pattern.compile(matcher);
        }
    }
}


