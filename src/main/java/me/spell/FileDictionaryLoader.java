package me.spell;

import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * A {@link DictionaryLoader} backed by a file.
 * Currently the file parser follows the following format:
 *
 * {@literal
 * word1 freq1
 * word2 freq2
 * ...
 * }
 *
 * {@literal word} should be parseable as {@link String}, whereas {@literal freq} should be parseable as {@link Float}
 *
 * @author bruno
 */
public class FileDictionaryLoader implements DictionaryLoader {
    private final ImmutableMap<String, Float> dict;

    public FileDictionaryLoader(BufferedReader input) throws IOException {
        dict = parseFile(input);
    }

    @Inject
    public FileDictionaryLoader(@Named("dictionary") File db) throws IOException {
       this(new BufferedReader(new FileReader(db)));
    }

    private static ImmutableMap<String, Float> parseFile(BufferedReader input) throws IOException {
        ImmutableMap.Builder<String, Float> dict = ImmutableMap.builder();

        // We assume a column-based format, in which columns are separated by one or more whitespace characters.
        // The first column contains the word, the second (optional) column holds the frequency for that word.
        String line;
        while ((line = input.readLine()) != null) {
            String[] tokens = line.split("\\s+", 3);
            String word = tokens[0];
            Float freq = 0.0F;
            try { freq = Float.parseFloat(tokens[1]); }
            catch (RuntimeException e) {
                // TODO: log warning, continue with default value
            }

            dict.put(word, freq);
        }

        return dict.build();
    }

    /**
     * {@inheritDoc}
     * The returned map is immutable, hence thread safe.
     */
    @Override
    public ImmutableMap<String, Float> load() {
        return dict;
    }
}
