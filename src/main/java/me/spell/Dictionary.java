package me.spell;

/**
 * @author bruno
 */
public interface Dictionary {
    /**
     * Check if a word exists in the dictionary
     * @param word
     * @return true if the word exists as-is in the dictionary
     */
    public boolean contains(String word);

    /**
     * Get the frequency of occurrence of a word.
     *
     * The values returned could be interpreted as a probability of occurrence, but it is not enforced. In other words,
     * the sum of frequencies of all words need not add to {@value 1.0}.
     *
     * If {@literal word} is not in the dictionary, then the value returned is {@value 0.0F}
     * @param word
     * @return a value that correlates with the probability of occurrence of {@literal word}
     */
    public float frequency(String word);
}
