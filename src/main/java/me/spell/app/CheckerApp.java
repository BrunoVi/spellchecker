package me.spell.app;

import dagger.ObjectGraph;
import me.spell.SpellChecker;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.System.in;
import static java.lang.System.out;

/**
 * @author bruno
 */
public class CheckerApp implements Runnable {
    private final SpellChecker checker;

    public static void main(String[] args) throws IOException {
        if (args == null || args[0] == null) throw new IllegalArgumentException("Need a dictionary filename.");
        CheckerApp checker = ObjectGraph.create(new SimpleCheckerModule(args[0])).get(CheckerApp.class);
        checker.run();
    }

    @Inject public CheckerApp(SpellChecker checker) {
        this.checker = checker;
    }

    @Override
    public void run() {
        try (BufferedReader tty = new BufferedReader(new InputStreamReader(in))) {
            String line;
            while ((line = tty.readLine()) != null) {
                for (String misspelled : checker.checkText(line))
                    out.println(misspelled + " " + checker.suggest(misspelled));
            }
        } catch (IOException e) {
            out.println("Error reading user input: " + e.getMessage());
        }
    }
}

