package me.spell.app;

import dagger.Module;
import dagger.Provides;
import me.spell.*;

import javax.inject.Named;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * @author bruno
 */
@Module(entryPoints = {CheckerApp.class, SpellChecker.class})
public class SimpleCheckerModule {
    private final File dictionaryFile;

    public SimpleCheckerModule(String dictionaryFileName) throws IOException {
        this.dictionaryFile = new File(checkNotNull(dictionaryFileName));

        if (!dictionaryFile.exists())
            throw new FileNotFoundException(format("File %s does not exist.", dictionaryFileName));

        if (!dictionaryFile.canRead())
            throw new IOException(format("File %s is not readable.", dictionaryFileName));
    }

    @Provides Dictionary provideDictionary(InMemoryDictionary dictionary) {
        return dictionary;
    }

    @Provides DictionaryLoader provideDictionaryLoader(FileDictionaryLoader loader) {
        return loader;
    }

    @Provides @Named("dictionary") File provideDictionaryFile() {
        return dictionaryFile;
    }
}
