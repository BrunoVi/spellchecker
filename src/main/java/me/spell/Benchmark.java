package me.spell;
import com.google.caliper.Param;
import com.google.caliper.SimpleBenchmark;
import dagger.ObjectGraph;
import me.spell.app.SimpleCheckerModule;
import java.io.IOException;
import java.util.List;

/**
 * @author bruno
 */
public class Benchmark extends SimpleBenchmark {
    @Param({"foh", "applez", "pickpickpick", "supercalifragilisticuespialidoso"}) String word;
    static SpellChecker checker = getChecker();

    public List<String> timeSuggest(int reps) {
        List<String> suggestions = null;
        for (int i = 0; i < reps; i++) {
           suggestions = checker.suggest(word);
        }

        return suggestions;
    }

    static SpellChecker getChecker() {
        SpellChecker checker = null;
        try { checker = ObjectGraph.create(new SimpleCheckerModule("/usr/share/dict/words")).get(SpellChecker.class); }
        catch (IOException e) { e.printStackTrace(); System.exit(1); }

        return checker;
    }
}
