package me.spell;

import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;

/**
 * A {@link Dictionary} that fits in memory.
 *
 * @author bruno
 */
public class InMemoryDictionary implements Dictionary {
    private final ImmutableMap<String, Float> words;

    @Inject
    public InMemoryDictionary(DictionaryLoader loader) {
        this.words = ImmutableMap.copyOf(loader.load());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(String word) {
        return words.containsKey(word);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public float frequency(String word) {
        Float freq = words.get(word);
        return freq == null ? 0F : (float)freq;
    }
}
