package me.spell;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ComparisonChain;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author bruno
 */
public class SpellChecker {
    private final Dictionary dictionary;
    private final WordParser parser;

    @Inject
    public SpellChecker(Dictionary dictionary, WordParser parser) {
        this.dictionary = checkNotNull(dictionary);
        this.parser = checkNotNull(parser);
    }

    /**
     * Checks the spelling of a word
     *
     * @param word the word to check
     * @return true if the word is correctly spelled
     */
    public boolean isMisspelled(String word) {
        word = word.toLowerCase();
        return !dictionary.contains(word) && !word.equals("");
    }

    public @Nonnull Set<String> checkText(String text) {
        return misspelled(parser.parseText(text));
    }

    /**
     * Suggest corrections for a misspelled word
     *
     * @param misspelledWord the word to check
     * @return a (possibly empty) list of correctly-spelled words
     */
    public @Nonnull List<String> suggest(String misspelledWord) {
        ArrayList<String> suggestions = new ArrayList<>();
        if (dictionary.contains(misspelledWord) || misspelledWord.equals("")) return suggestions;

        suggestions.addAll(generateSuggestions(misspelledWord));

        Collections.sort(suggestions, BY_FREQUENCY_DESC);

        return suggestions;
    }

    @VisibleForTesting @Nonnull Set<String> misspelled(Collection<String> words) {
        Set<String> misspelled = new HashSet<>();
        for (String word : words) if (isMisspelled(word)) misspelled.add(word);

        return misspelled;
    }

    /**
     * Returns modifications of a word by a single transposition, insertion, deletion or alteration.
     *
     * @param word
     * @return A set of words reachable by one operation over the misspelled word
     */
    private Set<String> generateSuggestions(String word) {
        Set<String> variants = new HashSet<>();

        for (int i = 0; i < word.length(); i++) {
            // deletions
            String d = new StringBuilder(word).deleteCharAt(i).toString();
            if (dictionary.contains(d)) variants.add(d);

            // transpositions (swap two consecutive characters)
            if (i + 1 < word.length()) {
                char[] variant = word.toCharArray();
                swap(variant, i, i+1);
                String tr = new String(variant);
                if (dictionary.contains(tr)) variants.add(tr);
            }
        }

        for (int i = 0; i <= word.length(); i++)
            // XXX: should be using the parser's charset
            for (char c = 'a'; c <= 'z'; c++) {
                // insertions, including prepend (i = 0) and append (i = word.length())
                String ins = new StringBuilder(word).insert(i, c).toString();
                if (dictionary.contains(ins)) variants.add(ins);
                // replacing
                if (i < word.length()) {
                    StringBuilder sb = new StringBuilder(word);
                    sb.setCharAt(i, c);
                    String rep = sb.toString();
                    if (dictionary.contains(rep)) variants.add(rep);
                }
            }

        return variants;
    }

    private static void swap(char[] variant, int i, int j) {
        char temp = variant[i];
        variant[i] = variant[j];
        variant[j] = temp;
    }

    private final Comparator<String> BY_FREQUENCY_DESC = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return ComparisonChain.start().compare(dictionary.frequency(o2), dictionary.frequency(o1)).result();
        }
    };
}
