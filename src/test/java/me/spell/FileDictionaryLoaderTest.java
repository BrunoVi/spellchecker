package me.spell;

import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import static junit.framework.TestCase.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author bruno
 */
public class FileDictionaryLoaderTest {
    BufferedReader mockReader = mock(BufferedReader.class);
    Map<String, Float> testWords;
    Map<String, String> badWords;

    @Before
    public void setUp() throws IOException {
        testWords = Maps.newHashMap();
        badWords = Maps.newHashMap();

        testWords.put("puss", 0.1F);
        testWords.put("with", 0.01F);
        testWords.put("boots", 0.05F);

        OngoingStubbing<String> stub = when(mockReader.readLine());

        for (Map.Entry<String, Float> word : testWords.entrySet())
            stub = stub.thenReturn(word.getKey() + "  " + word.getValue());

        badWords.put("noFrequency", "");
        badWords.put("badFrequency", "number");

        for (Map.Entry<String, String> word : badWords.entrySet())
            stub = stub.thenReturn(word.getKey() + "  " + word.getValue());

        stub.thenReturn(null);
    }

    @Test
    public void testLoad() throws Exception {
        DictionaryLoader loader = new FileDictionaryLoader(mockReader);
        Map<String, Float> dict = loader.load();

        assertEquals(testWords.size() + badWords.size(), dict.size());

        for (String word : testWords.keySet()) {
            assertTrue(dict.containsKey(word));
            assertEquals(testWords.get(word), dict.get(word));
        }

        // Words that had missing or corrupt frequencies should get 0.0
        for (String word : badWords.keySet()) {
            assertTrue(dict.containsKey(word));
            assertEquals(0.0F, dict.get(word));
        }

        assertFalse(dict.containsKey("foo"));
    }
}
