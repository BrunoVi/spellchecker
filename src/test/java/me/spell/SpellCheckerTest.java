package me.spell;

import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;
import static junit.framework.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author bruno
 */
public class SpellCheckerTest {
    SpellChecker checker;

    @Before
    public void setUp() throws Exception {
        Map<String, Float> table = new HashMap<>();

        table.put("apple", 0.1F);
        table.put("apples", 0.05F);

        DictionaryLoader mockLoader = mock(DictionaryLoader.class);
        when(mockLoader.load()).thenReturn(table);

        checker = new SpellChecker(new InMemoryDictionary(mockLoader), new WordParser());
    }

    @Test
    public void testIsMisspelled() throws Exception {
        assertTrue(checker.isMisspelled("aple"));
        assertFalse(checker.isMisspelled("apple"));
    }

    @Test
    public void testSuggest() throws Exception {
        assertEquals(asList("apple", "apples"), checker.suggest("applez"));
        assertEquals(asList(), checker.suggest("apple"));
        assertEquals(asList(), checker.suggest(""));
        assertEquals(asList(), checker.suggest("1)"));
    }

    @Test
    public void testMisspelled() throws Exception {

        Set<String> misspelled = Sets.newHashSet("aple", "applez");
        assertEquals(misspelled, checker.misspelled(misspelled));
    }
}
