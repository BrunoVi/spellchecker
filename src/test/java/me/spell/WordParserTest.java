package me.spell;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author bruno
 */
public class WordParserTest {
    WordParser english;

    @Before
    public void setEnglish() {
        english = new WordParser(WordParser.CharSet.ENGLISH);
    }

    @Test
    public void testParseText() throws Exception {
        String sentence = "The cat,as it turns out, is sleep-deprived now.";
        List<String> tokens = english.parseText(sentence);
        assertEquals(10, tokens.size());

        assertEquals("the", tokens.get(0));
        assertEquals("cat", tokens.get(1));
        assertEquals("now", tokens.get(tokens.size() - 1));
    }
}
