package me.spell;

import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author bruno
 */
public class InMemoryDictionaryTest {
    DictionaryLoader mockLoader = mock(DictionaryLoader.class);
    Dictionary dictionary;

    @Before
    public void setUp() {
        Map<String, Float> dict = Maps.newHashMap();

        dict.put("the", 0.1F);
        dict.put("house", 0.01F);
        dict.put("a", 0.15F);

        when(mockLoader.load()).thenReturn(dict);

        dictionary = new InMemoryDictionary(mockLoader);
    }

    @Test
    public void testContains() throws Exception {
        for (String word : mockLoader.load().keySet())
            assertTrue(dictionary.contains(word));
    }

    @Test
    public void testFrequency() throws Exception {
        for (Map.Entry<String, Float> entry : mockLoader.load().entrySet())
            assertEquals(entry.getValue(), (Float)dictionary.frequency(entry.getKey()));

        for (String madeUpWord : new String[]{ "foo", "bar", "baz"})
            assertEquals((Float)0.0F, (Float)dictionary.frequency(madeUpWord));
    }
}
